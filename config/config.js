//puerto
process.env.PORT = process.env.PORT || 4000

//token
//vencimiento del token
process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30
//SEED de autenticacion
process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo'

//base de datos
process.env.URLDB = process.env.URLDB || 'mongodb://localhost:27017/manusecurity'