const {Router} = require('express');
const router = Router();
//const express = require('express');
//const router = express.Router();
const Report = require('../models/Report')

router.route('/').get(async (req, res)=>{
    const reports = await Report.find();
    res.json(reports);
});
router.route('/newreport').post((req, res)=>{
    const { title, desc, user, img } = req.body;
    const newReport = new Report({
        title,
        desc,
        user,
        img
    });
    newReport.save((err, report)=>{
        if(err){
            res.status(500).json({
                err
            });
        } else{
            res.json(report)
        }
    })
});
router.route('/:id')
    .get((req, res)=>{
        const id = req.params.id;
        Report.findById(id, (err, report)=>{
            if(err){
                res.status(500).json({
                    err
                });
            } else{
                res.json(report);
            }
        });
    })
    .put((req, res)=>{
        const { title, desc, user, img } = req.body;
        Report.findByIdAndUpdate(req.params.id, { title, desc, user, img }, (err, report)=>{
            if(err){
                res.status(500).json({
                    err
                });
            } else{
                res.json(report);
            }
        })
    })
    .delete((req, res)=>{
        Report.findByIdAndDelete(req.params.id, (err, report)=>{
            if(err){
                res.status(500).json({
                    err
                });
            } else{
                res.json(report);
            }
        })
    })

module.exports = router;