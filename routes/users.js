const { Router } = require('express');
const router = Router();
const { protect } = require("../controllers/userMidleware");
const User = require('../models/User')

router.route('/').get(async (req, res) => {
    const users = await User.find();
    res.json(users);
});
router.route('/register').post(async (req, res) => {
    const { username, email, password } = req.body;
    const newUser = new User({
        username,
        email,
        password,
        img: 'avatar.png'
    });
    newUser.password = await newUser.encryptPassword(password);
    newUser.save((err, user) => {
        if (err) {
            res.status(500).json({
                ok: false,
                err
            });
        } else {
            let token = newUser.newToken();
            res.json({
                ok: true,
                message: 'Se ha registrado correctamente',
                token,
                user
            })
        }
    })
});
router.route('/login').post(async (req, res) => {
    const { email, password } = req.body;
    const user = await User.findOne({email: email});
    if (!user) {
        res.json({
            ok: false,
            message: 'Credenciales incorrectas',
        })
    } else {
        const match = await user.matchPassword(password);
        if(match) {
            const token = user.newToken();
            res.json({
                ok: true,
                message: 'Se ha logueado correctamente',
                token,
                user
            })
        } else {
            res.json({
                ok: false,
                message: 'Credenciales incorrectas',
            })
        }
    }
});
router.route('/:id')
    .get(protect, (req, res) => {
        const id = req.params.id;
        User.findById(id, (err, user) => {
            if (err) {
                res.status(500).json({
                    err
                });
            } else {
                res.json(user);
            }
        });
    })
    .put((req, res) => {
        const { username, email, img } = req.body;
        User.findByIdAndUpdate(req.params.id, { username, email, img }, (err, user) => {
            if (err) {
                res.status(500).json({
                    err
                });
            } else {
                res.json(user);
            }
        })
    })
    .delete((req, res) => {
        User.findByIdAndDelete(req.params.id, (err, user) => {
            if (err) {
                res.status(500).json({
                    err
                });
            } else {
                res.json(user);
            }
        })
    })

module.exports = router;