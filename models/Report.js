const {Schema, model} = require('mongoose');

const reportSchema = new Schema({
    title: {
        unique: true,
        type: String,
        require: true
    },
    desc: {
        type: String,
        require: true
    },
    user: {
        type: String,
        require: true
    },
    img: {
        type: String,
        required: false,
    },
},{
    timestamps: true
});

module.exports = model('Report', reportSchema);