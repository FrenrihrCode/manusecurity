const {Schema, model} = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");

const jwt_secret = process.env.SEED;
const jwt_expiry_time = "3h";

const userSchema = new Schema({
    username: {
        type: String,
        unique: [true, "El nombre ya esta en uso"],
        required: [true, "El nombre es requerido"]
    },
    email: {
        type: String,
        unique: [true, "El correo ya esta en uso"],
        required: [true, "El correo es requerido"]
    },
    password: {
        type: String,
        required: [true, "El password es requerido"]
    },
    img: {
        type: String,
    }
},{
    timestamps: true
});

userSchema.methods.encryptPassword = async password => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
};
  
userSchema.methods.matchPassword = async function(password) {
    return await bcrypt.compare(password, this.password);
};

userSchema.methods.newToken = function() {
    return jwt.sign({ _id: this._id }, jwt_secret, {
      expiresIn: jwt_expiry_time
    });
};

module.exports = model('User', userSchema);