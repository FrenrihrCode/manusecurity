const express = require('express');
const helmet = require("helmet");
const cors = require('cors');
const socketio = require('socket.io');
const http = require('http');
const bodyParser = require('body-parser');
//importar conf
require('./config/config');
//configurar el servidor
const bodyParserJSON = bodyParser.json();
const bodyParserURLEncoded = bodyParser.urlencoded({ extended: true });
const app = express();
//middleware
app.use(bodyParserJSON);
app.use(bodyParserURLEncoded);
app.use(helmet());
app.use(cors());
//sockets
const server = http.createServer(app);
const io = socketio(server);
//funciones de la clase usuario
const { addUser, removeUser, getUser, getUsersInRoom } = require('./utils/user');
//eventos del socket
io.on('connection', (socket) => {
  console.log('Usuario conectado');
  socket.on('join', ({ name, room }, callback) => {
    const { error, user } = addUser({ id: socket.id, name, room });
    var fecha = new Date();
    var hora = fecha.getHours()+":"+("0"+fecha.getMinutes()).slice(-2)
    if (error) return callback(error);
    socket.join(user.room);
    socket.emit('message', { user: 'servidor', text: `${user.name}, bienvenido al chat ${user.room}, disfruta tu estancia.` });
    socket.broadcast.to(user.room).emit('message', { user: 'servidor:', text: `${user.name} se ha unido!`, hora:hora });
    io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });
    callback();
  });

  socket.on('sendMessage', (message, callback) => {
    var fecha = new Date();
    var hora = fecha.getHours()+":"+("0"+fecha.getMinutes()).slice(-2)
    const user = getUser(socket.id);
    io.to(user.room).emit('message', { user: user.name, text: message, hora:hora });
    callback();
  });

  socket.on('disconnect', () => {
    console.log('Un usuario ha salido');
    const user = removeUser(socket.id);
    var fecha = new Date();
    var hora = fecha.getHours()+":"+("0"+fecha.getMinutes()).slice(-2)
    if (user) {
      io.to(user.room).emit('message', { user: 'servidor:', text: `${user.name} salió!.`, hora:hora });
      io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });
    }
  });

  socket.on('typing', (data)=>{
    let user = getUser(socket.id);
    if(data.typing==true)
      socket.broadcast.to(user.room).emit('display', data)
    else
      socket.broadcast.to(user.room).emit('display', data)
  })
  /*
  console.log('Usuario conectado');
    //client.on('entrarChat', (usuario) => console.log("Usuario conectado: ", usuario));
    client.on('entrarChat', (data, callback)=>{
        if(!data.nombre || !data.sala){
            return callback({
                error: true,
                mensaje: "El nombre/sala es necesario"
            });
        }
        //unir a una sala
        client.join(data.sala);
        usuarios.agregarPersona(client.id, data.nombre, data.sala);
        //client.broadcast.emit("listaPersonas", usuarios.getPersonas());
        client.broadcast.to(data.sala).emit('listaPersonas', usuarios.getPersonasPorSala(data.sala));
        callback(usuarios.getPersonasPorSala(data.sala));
    });

    client.on('crearMensaje', (data, callback) => {
        let persona = usuarios.getPersona(client.id);
        let mensaje = crearMensaje(persona.nombre, data.mensaje);
        client.broadcast.to(persona.sala).emit("crearMensaje", mensaje);
        callback(mensaje);
    })

    client.on('disconnect', () =>{
        let personaBorrada = usuarios.borrarPersona(client.id);
        client.broadcast.to(personaBorrada.sala).emit('crearMensaje', crearMensaje('Admin', `${personaBorrada.nombre} ha salido.`));
        client.broadcast.to(personaBorrada.sala).emit("listaPersonas", usuarios.getPersonas());
    })
    
    client.on('mensajePrivado', (data)=> {
        let persona = usuarios.getPersona(client.id);
        client.broadcast.to(data.para).emit('mensajePrivado', crearMensaje(persona.nombre, data.mensaje));
    })*/
});

app.get('/', (req, res) => {
  res.send({ response: "Server is up and running." }).status(200);
});
//database
const mongoose = require('mongoose');
mongoose.connect(process.env.URLDB, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false
}, (err, des) => {
  if (err) throw err;
  else console.log('Base de datos online');
});
//routes
app.use('/api/users', require('./routes/users'));
app.use('/api/reports', require('./routes/reports'));
server.listen(process.env.PORT, () => console.log(`App listening on port ${process.env.PORT}!`));