const mongoose = require("mongoose");
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const jwt_secret = process.env.SEED;

const protect = async (req, res, next) => {
  const bearer = req.headers.authorization;
  if (!bearer || !bearer.startsWith("Bearer ")) {
    return res.status(401).json({
        err: 'No se encuentra autorizado para realizar esta acción'
    });
  }
  const token = bearer.split("Bearer ")[1].trim();
  try {
    const payload = await jwt.verify(token, jwt_secret);
    const user = await User.findById(payload._id).select({ password: 0 });
    req.user = user.toJSON();
    console.log(req.user)
    next();
  } catch (error) {
    console.error(error);
    return res.status(401).json({
        err: 'No se encuentra autorizado para realizar esta acción'
    });
  }
};

module.exports = {
  protect
};